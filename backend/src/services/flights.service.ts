import {Types} from 'mongoose'

import { FlightsModel, Flight } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return await FlightsModel.find()
    }
    async update(id: Types.ObjectId, flightData: Flight){
        return await FlightsModel.updateOne({_id: id}, flightData);
    }
}
