import { JsonController, Get, Put, Param, Body } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'
import {Flight} from '../models/flights.model'

const flightsService = new FlightsService();

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }
    @Put('/:id')
    async put(@Param('id') id: string, @Body() flight: Flight) {
        return {
            status: 200,
            data: await flightsService.update(id, flight)
        }
    }
}
